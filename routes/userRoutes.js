

const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js");
// Route for checking if the user's email exists in database

router.post("/checkEmail", (req,res)=>{
	userController.checker(req.body).then(checkerResult => res.send(checkerResult));
});


// route for user registration

router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(result => res.send(result));
});

//route for user authentication

router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(result=>{
		res.send(result);
	});
});

router.get("/details", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId: userData.id}).then(result=>{
		res.send(result);
	});
});

router.post("/enroll", auth.verify, (req,res)=> {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId: userData.id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result));
})



module.exports = router;