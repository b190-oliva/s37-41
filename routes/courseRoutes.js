const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");

// Route for creating a course

router.post("/", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin != true){
		res.send(`Authorization failed`);
	}
	else{
		courseController.addCourse(req.body).then(result => {
			res.send(result);
		})
	}
});

// router.post("/", (req,res)=>{
// 	const isAdmin = auth.decode(req.headers.authorization);
// 	courseController.addCourse({isAdmn});
// });

// route for getting all courses

router.get("/all", (req,res)=>{
	courseController.getAllCourses().then(result => res.send(result));
});

// route for getting only active courses

router.get("/active", (req,res)=>{
	courseController.getAllActive().then(result => res.send(result));
});

//route for getting specific course
router.get("/:courseId", (req,res)=>{
	courseController.getCourse(req.params).then(result=> res.send(result));
});

//route for updating a specific course using id
router.put('/:courseId', auth.verify, (req, res) =>{
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// ======= ACTIVITY ========

// route for archiving a course

router.put("/:courseId/archive", auth.verify, (req,res) => {
	console.log(res);
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin != true){
		res.send(`Authorization failed`);
	}
	else{
		courseController.archiveCourse(req.params, req.body).then(result => res.send(result));
		}
});

module.exports = router;
