const jwt = require("jsonwebtoken");

// used in algorithm for encrypting our data which makes it difficult to decode
const secret = "password"

module.exports.createAccessToken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret, {});
};

// Token verification
module.exports.verify = (req,res,next) =>{
	// token is retrieved from the request headers
	let token = req.headers.authorization;
	if(typeof token !== "undefined"){
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err,data) => {
			if(err){
				return res.send({auth: "failed"});
			}
			else{
				next();
			}
		})
	}
	else {
		return res.send({auth: "failed"});
	};
};

// Token decryption

module.exports.decode = (token) => {
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token,secret, (err,data) =>{
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
				// .payload - contains the information in the createAccessToken method defined above (values for id, isAdmin, email)
			};
		});
	}
	else {
		// if the token does not exist
		return null;
	}
};


