 

const User = require("../models/user.js");
const Course = require("../models/course.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checker = (reqbody) => {
	return User.find({ email: reqbody.email}).then(result =>{
		if(result.length > 0){
			return true;
		}
		else{
			return false;
		};
	});
};

module.exports.registerUser = (reqbody) => {
	let newUser = new User({
		firstname: reqbody.firstname,
		lastname: reqbody.lastname,
		email: reqbody.email,
		mobilenumber: reqbody.mobilenumber,
		// hashSync - bcrypt's method for encrypting the password of the user once they have succesfully registered in our database
		/*
			first parameter - the value needs to be encrypted
			second parameter 
			3\
			]- (number/int)dictates how may "salt" routes to be given to encrypt the value
		*/
		password: bcrypt.hashSync(reqbody.password, 10),
		isAdmin: reqbody.isAdmin
	})
	return newUser.save(reqbody).then((result,error) =>{
		if(error){
			return false;
		}
		else{
			return true;
		};
	});
};

// function for login

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result.length>0){
			return true;
		}
		else {
			const isPwCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPwCorrect){
				return {access: auth.createAccessToken(result)}
			}
			else
			{
				return false;
			}
		};
	});
};

// module.exports.getProfile = (userDetails) => {
// 	console.log(userDetails);
// 	return User.findOne({id: userDetails.id}).then(result =>{
// 		if(userDetails.id === result.id){
// 			const hide = "";
// 			result.password = hide;
// 			return result;
// 		}
// 		else if(userDetails.id.length <= 0){
// 			return `Please enter a user id`
// 		}
// 		else {
// 			return `invalid user id`
// 		}
// 	});
// };

module.exports.getProfile = (data) => { 
	console.log(data);
	return User.findById(data.userId).then(result => { 
	// Changes the value of the user's password to an empty string when returned to the frontend // Not doing so will expose the user's password which will also not be needed in other parts of our application // Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application 
		result.password = ""; 
		// Returns the user information with the password as an empty string 
		return result; 
	}); 
};


// function to enroll a user to a course

module.exports.enroll = async (data) =>{
	// adding the courseId in the enrollments array of the user
	// returns boolean depending if the updating of the document is successful (true) or failed (false)
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({courseId: data.courseId});

		// saves the updated user information in the database
		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			}
		})
	})
	/*
		try to update the enrollees array in the course documents using the codes above as your guide
		10 minutes: 6:03 pm (do not create any Postman requests yet)
	*/
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding of userId in the enrollees array
		course.enrollees.push({userId: data.userId});

		// saves the updated course information in the database
		return course.save().then((course, error)=>{
			if (error) {
				return false;
			}else{
				return true;
			}
		})
	});

	// condition that will check if the user and course documents have been updated
	if (isUserUpdated && isCourseUpdated) {
		// User enrollment successful
		return true;
	}else{
		//User enrollement failed
		return false; 
	}

}
