const Course = require("../models/course.js");


module.exports.addCourse = (reqbody) => {
	console.log(reqbody);
	let newCourse = new Course({
	name: reqbody.name,
	description: reqbody.description,
	price: reqbody.price
	})
	return newCourse.save(reqbody).then((result,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};

module.exports.getAllCourses = () => {
	return Course.find({}).then(result =>{
		if(result.length > 0){
			return result;
		}
		else{
			return false;
		};
	});
};

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {return result});
};

module.exports.getCourse = (req) => {
	return Course.findById(req.courseId).then(result => {return result});
};

module.exports.updateCourse = (params,update) => {
	let updateCourse = {
			name: update.name,
			description: update.description,
			price: update.price,
		}
	return Course.findByIdAndUpdate(params.courseId, updateCourse).then((result,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};

// ======= ACTIVITY ========

//removed reqbody
module.exports.archiveCourse = (req) => {
	let updateActiveField = {
			isActive: false
		}
	return Course.findByIdAndUpdate(req.courseId, updateActiveField).then((result,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	});
};