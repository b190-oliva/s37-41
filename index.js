

const express = require("express");
const mongoose = require("mongoose");
// allows us to control the app's cross origin resources sharing settings
const cors = require("cors");

const app = express();

// routes import

const userRoutes = require("./routes/userRoutes.js");
const courseRoutes = require("./routes/courseRoutes.js");

mongoose.connect("mongodb+srv://39dexshaman:Chowking123@wdc028-course-booking.8f4g3cd.mongodb.net/b190-Course-Booking?retryWrites=true&w=majority",
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the database"));

// cors allow all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/courses", courseRoutes);

//process.env.PORT handles the environment of the hosting websites should our app will be hosted in a website such as Heroku
app.listen(process.env.PORT || 4000, () => {console.log(`API now online at port ${process.env.PORT || 4000}`)});

